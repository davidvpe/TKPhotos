//
//  SquareTransitionAnimation.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit

class PresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var openingFrame: CGRect?
    var sourceImage : UIImage?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let containerView = transitionContext.containerView()
        
        let animationDuration = self .transitionDuration(transitionContext)
        
        let snapshotView = UIView()
        snapshotView.frame = toViewController.view.bounds
        snapshotView.backgroundColor = .blackColor();
        snapshotView.alpha = 0;
        containerView!.addSubview(snapshotView)
        
        let imageView = UIImageView(image: sourceImage)
        imageView.contentMode = .ScaleAspectFit
        imageView.frame = openingFrame!
        containerView!.addSubview(imageView)
        
        toViewController.view.alpha = 0.0
        containerView!.addSubview(toViewController.view)
        
        UIView.animateWithDuration(animationDuration, animations: { 
            imageView.frame = fromViewController.view.frame
            snapshotView.alpha = 1.0
        }) { (finished) -> Void in
            
            toViewController.view.alpha = 1.0
            imageView.removeFromSuperview()
            snapshotView.removeFromSuperview()
            transitionContext.completeTransition(finished)
        }
        
        
    }
}