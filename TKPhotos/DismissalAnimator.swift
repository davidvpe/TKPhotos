//
//  DismissalAnimator.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit

class DismissalAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var openingFrame: CGRect?
    var sourceImage : UIImage?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        
        let containerView = transitionContext.containerView()
        
        let animationDuration = self .transitionDuration(transitionContext)
        
        let snapshotView = UIView()
        snapshotView.frame = fromViewController.view.bounds
        snapshotView.backgroundColor = .blackColor();
        snapshotView.alpha = 1;
        containerView!.addSubview(snapshotView)
        
        let imageView = UIImageView(image: sourceImage)
        imageView.contentMode = .ScaleAspectFit
        imageView.frame = fromViewController.view.bounds
        containerView!.addSubview(imageView)
        
        //containerView!.addSubview(snapshotView)
        
        fromViewController.view.alpha = 0.0
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            imageView.frame = self.openingFrame!
            snapshotView.alpha = 0;
        }) { (finished) -> Void in
            imageView.removeFromSuperview()
            snapshotView.removeFromSuperview()
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
    }
}