//
//  ViewController.swift
//  TKPhotos
//
//  Created by David Velarde on 8/26/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit


class MainViewController: UIViewController {

    @IBOutlet weak var collectionPhotos: UICollectionView!
    
    var imagesArray : [TKPhoto]?
    let transitionDelegate: TransitioningDelegate = TransitioningDelegate()
    var pictureSelected : UIImage?
    var pictureName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "TKPhotos"
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        imagesArray = TKDataAccess.sharedInstance.getAllImages()
        collectionPhotos.reloadData()
    }

    func getImageForIndexPath(indexPath: NSIndexPath) -> UIImage?{
        
        let imageUdid = self.imagesArray![indexPath.item].udid
        
        let image = TKDataAccess.sharedInstance.getUIImageFromUdid(imageUdid)
        
        return image
    }
    
    // MARK: - UICollectionViewDataSource
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrayPictures = imagesArray
        {
            return arrayPictures.count
        }
        else{
            return 0;
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "PhotoCell"
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PhotoCell
        cell.backgroundColor = .whiteColor()
        
        cell.imgPhoto.image = getImageForIndexPath(indexPath)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if let image = getImageForIndexPath(indexPath){
            
            self.pictureSelected = image
            self.pictureName = self.imagesArray![indexPath.item].name
            
            let attributes = collectionView.layoutAttributesForItemAtIndexPath(indexPath)
            let attributesFrame = attributes?.frame
            let frameToOpenFrom = collectionView.convertRect(attributesFrame!, toView: collectionView.superview)
            transitionDelegate.openingFrame = frameToOpenFrom
            transitionDelegate.sourceImage = image
            self.performSegueWithIdentifier("fromMainToPhotoDetail", sender: nil)
        }
        
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize = collectionView.bounds
        let desiredWidth : CGFloat = 100.0
        var desiredDivisions = NSInteger(screenSize.width / desiredWidth)
        if screenSize.width % desiredWidth != 0
        {
            desiredDivisions = desiredDivisions + 1
        }
        let divisions : CGFloat = CGFloat(desiredDivisions)
        let perfectSide = screenSize.width/divisions
        return CGSizeMake(perfectSide, perfectSide)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? PhotoDetailViewController{
            
            detailVC.transitioningDelegate = transitionDelegate
            detailVC.modalPresentationStyle = .Custom
            
            detailVC.picture = self.pictureSelected;
            detailVC.pictureName = pictureName
            detailVC.sourceType = TKDetailType.FromMain
        }
    }
    

}

